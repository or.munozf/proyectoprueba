require('dotenv').config();
require('./database/db')
const Koa = require('koa');
const app = new Koa();
var cors = require('koa-cors');
const bodyParser = require('koa-bodyparser');
const Router = require('koa-router')
const router = new Router();
const controller = require ('./controller/peliculaController');


const PORT = process.env.PORT || 5000
//middle
app.use(router.routes());
app.use(bodyParser());


//rutas
router.get('/api/v0/oneFilm/:id', controller.oneFilm)
router.get('/api/v0/allFilms',controller.allFilms)
router.post('/api/v0/registerFilms',controller.registerFilms)
router.put('/api/v0/updateFilms/:id',controller.updateFilms)



app.listen(PORT,()=>{
    console.log(`backend montado con exito en el puerto ${PORT}`)
})

