const Peliculas = require("../model/peliculaModel");



const oneFilm = async(ctx) => {
    try {
        const id = ctx.params.id
        if(id.length === 24){
            const oneFilm = await Peliculas.findById(id);
                if(!oneFilm){
                    ctx.body = "El id que busca no existe";
                }else{
                    ctx.body = oneFilm;
                }
    
        }else{
            ctx.body = "el id buscado no cumple con el requisito de 24 caracteres";
        }
    } catch (error) {
        console.log(error)
    }
};

const allFilms = async(ctx) => {
    try{
        const allUser = await Peliculas.find()
        ctx.body = allUser;
    }catch(error){
        console.log(error)
    }
};

const registerFilms = async (ctx) => {
    try {

        const {title,year,released,genre,director,actors,plot,ratings} = ctx.request.header
        const  titleUnique = await Peliculas.find({title});

        if(!title || !year || !released || !genre || !director || !actors || !plot || !ratings){
            return ctx.body = `uno o mas campos no estan agregados`;
        }else if (titleUnique.length !== 0) {
            return ctx.body = `ya existe una pelicula con el siguiente nombre ${title}`;
        }else{
            const newFilms = new Peliculas({title,year,released,genre,director,actors,plot,ratings});
            await newFilms.save()
                .then(() => {
                        ctx.body = `Se agrego la pelicula correctamente ${newFilms}`;
                })
                .catch((error) => console.error(error));
        };

    }catch (error) {
        console.error(error)
    }
};

const updateFilms = async (ctx) =>{
    try {
        const {title,year,released,genre,director,actors,plot,ratings} = ctx.request.header
        const  titleUnique = await Peliculas.find({title});
        const id = ctx.params.id    

        if(!title || !year || !released || !genre || !director || !actors || !plot || !ratings){
            return ctx.body = `desbes tener todos los campos llenados`;
            
        }else if (titleUnique.length !== 0) {
            return ctx.body = `ya existe una pelicula con el siguiente nombre ${title}`;
        }else{
            await Peliculas.findByIdAndUpdate({_id:id},ctx.request.header)
                .then(() => {
                    ctx.body = `Se actualizo correctamente la pelicula `;
                })
                .catch((error) => console.error(error));
        };
    } catch (error) {
        console.error(error)

    }
};

module.exports = {
    oneFilm,allFilms,registerFilms,updateFilms
}