const {model , Schema} = require("mongoose");

const peliculaSchema = new Schema ({
    title:String,
    year:Date ,
    released:String,
    genre:String,
    director:String,
    actors:String,
    plot:String,
    ratings:String
});

module.exports = model('Pelicula', peliculaSchema);